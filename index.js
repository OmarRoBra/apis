const express =require('express');  
const app=express();
const dotenv=require('dotenv');
const mongoose=require('mongoose');  
dotenv.config();
//exportar mi modulo para autenticacion.
const authRoute=require('./routes/auth');
const products=require('./routes/products');
const example=require('./routes/example');
 
//Midleware
app.use(express.json());
app.use(express.urlencoded({extended:false}));
//conección a la base de datos.
mongoose.connect(process.env.DB_CONNECT,{ useNewUrlParser: true,useUnifiedTopology: true  },()=>{
    console.log("Done");
});


//Cuando al app se encuentre en esta ruta se ejecutará la ruta ya establecida "Midlewares de rutas"
app.use('/api/user', authRoute);
app.use('/api/products',products);
app.use('/api/example',example);


//Se inicia la app en el puerto 3000
app.listen(3000, ()=> console.log('Server up')
);

