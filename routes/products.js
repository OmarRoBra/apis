const router=  require('express').Router();
const Product=require('../models/productModel');
const {productValidation}=require('../validation');

 
//Post en la ruta register
router.post('/add',async(req, res)=>{
   const {error}=productValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message); 
    const productExists = await Product.findOne({name: req.body.name});
     if(productExists) return res.status(400).send('Product exists');

    
   const product= new Product({ 
       name:req.body.name,
       amount:req.body.amount,
       typeOfproduct:req.body.typeOfproduct

   });
   try{
      const savedUser= await product.save();
      res.send({product:product.name});
   }catch (err){
         res.status(400).send(err); 
   }
});

router.get('/show', async(req,res)=>{
   try{
     const pToshow= await Product.find();
     res.send({status:'ok', data:pToshow});
   }catch(err){
      res.status(400).send(err);
   }
});

router.get('/search', async(req,res)=>{
   try{
     const pToshow= await Product.findOne({name:req.body.name});
     if(!pToshow) return res.status(400).send('Data no found');

     res.send({status:'ok', data:pToshow});

   }catch(err){
      res.status(400).send(err);
   }
});

router.post('/update', async(req,res)=>{

   try{
      const{name,amount,typeOfproductn,_id} =req.body;
      await Product.findOneAndUpdate(_id,{
       name,amount,typeOfproduct
      })
      res.send({status:'ok', message:'upadted'});
   }catch(err){
      res.status(400).send(err);
   }

});






module.exports=router;