const router=  require('express').Router();
const User=require('../models/userModel');
const bcrypt= require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerValidation,loginValidation}=require('../validation');
const verify= require('../verifyToken');

 
//Post en la ruta register
router.post('/register',async(req, res)=>{
   const {error}=registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message); 
    
    //Ver si el usuario ya está en la base de datos.
     const emailExists = await User.findOne({email: req.body.email});
     if(emailExists) return res.status(400).send('Email exists');

     //hash passwords
     const salt = await bcrypt.genSalt(10);
     const hashPassword = await bcrypt.hash(req.body.password, salt);
    
   const user= new User({ 
       name:req.body.name,
       email:req.body.email,
       password:hashPassword

   }); 
   try{
      const savedUser= await user.save();
      const token= jwt.sign({id:user._id},process.env.Token);
      res.status(200).send({user:user._id,token:token});
   }catch (err){
         res.status(400).send(err); 
   }
});

router.post('/login', async(req,res)=>{
     const {error}= loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    //comprobar si el usuario existe con el email. 
    const user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).send('Email exists');
    //Password is correct
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass) return res.status(400).send('Invalid');
   
    const token= jwt.sign({id:user._id},process.env.Token);
    res.header('auth-token', token).status(200).send({message:"Login",token:token});
   
    
});

router.get('/me',verify,async(req, res)=>{
  const xd=req.user.id
   try{
   const data= await User.findById({_id:xd});
   res.send({message:'ok',data:data})
   }catch(err){
      res.status(400).send(err);
   }
})




module.exports=router;