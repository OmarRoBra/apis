const mongoose= require('mongoose');
const productSchema =new mongoose.Schema({
    name:{
        type:String,
        required:true,
        min:6
    },
    amount:{
        type:Number,
        required:true,
    },
    typeOfproduct:{
        type:String,
        required:true,
        max:1024,
        min:6
    },
    date:{
        type:Date,
        default:Date.now
    }
    


});
module.exports=mongoose.model('productModel',productSchema);